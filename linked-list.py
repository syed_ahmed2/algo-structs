class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def print_list(self):
        printval = self.head
        while printval is not None:
            print(printval.data)
            printval = printval.next

    def add_beginning(self, new_data):
        new_node = Node(new_data)
        new_node.next = self.head
        self.head = new_node

    def add_end(self, new_data):
        new_node = Node(new_data)
        if self.head is None:
            self.head = new_node
            return

        last = self.head
        while last.next is not None:
            last = last.next

        last.next = new_node

    def add_between(self, middle_node, new_data):
        if middle_node is None:
            print("absent")
            return
        NewNode = Node(new_data)
        NewNode.next = middle_node.next
        middle_node.next = NewNode

    def remove(self, remove_key):
        head_val = self.head
        if head_val is not None:
            if head_val.data == remove_key:
                self.head = head_val.next
                head_val = None
                return

        while head_val is not None:
            if head_val.data == remove_key:
                break
            prev = head_val
            head_val = head_val.next

        if head_val == None:
            return

        prev.next = head_val.next
        head_val = None


L1 = LinkedList()
L1.head = Node("one")
e2 = Node("Tue")
e3 = Node("Wed")
# Link first Node to second node
L1.head.next = e2

# Link second Node to third node
e2.next = e3

L1.add_beginning("Sun")
L1.add_end("Thu")
L1.add_between(L1.head.next, "Fri")
L1.remove("Fri")

L1.print_list()
