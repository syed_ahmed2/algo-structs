# notes

bfs: time O(|E| + |V|). space O(|V|) = O(b^d)

E = edges

V = vertices

binary search: time O(logn) for worst/average case. best case O(1). space O(1)

bst: operations take O(N). space O(n)

dfs: time O(V+E). space length of longest path = m. store b nodes extra for
each of the m nodes. O(bm).

graph: time O(V + VlogE). space O(V+E)

hash table: time O(1). space O(n)

heap: time O(logn) worst. best case O(1). space O(1) w/ heapify. O(N) by
inserting all elements.

linked list: time O(1) if done on head, O(N) anywhere else. space O(1)

merge sort: time O(n*log(n)). space n

stack and queue: time insert/delete O(1). access/search O(N). space O(1)

trie: O(N * avgL). N = # of strings avgL = avergage length of N strings. space
O(N*k)

k = total # of unique characters in the alphabet
